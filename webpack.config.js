module.exports = {
    entry: "./examples/src/app.ts",
    output: {
        path: "./examples/build/",//__dirname,
        filename: "build.js"
    },
    module: {
        loaders: [
            { test: /\.ts$/, loader: "ts"},
            //{ test: /\.ts/, loader: "jsx2js!ts" },
            { test: /\.css$/, loader: "style!css" }
        ]
    },
    resolve: {
        extensions: ["", ".webpack.js", ".web.js", ".js", ".ts"]
    },

//    watch:[".ts",".js"],
	
    devtool: 'source-map'
};