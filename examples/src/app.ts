/// <reference path="../../lib/module.d.ts" />

import React=require ("../../../reactts/lib/ReactTS")
import LinearLayout =require ("../../lib/layouts/LinearLayout");
import MultiFrameLayout =require ("../../lib/layouts/MultiFrameLayout");
import ListLayout =require ("../../lib/layouts/ListLayout");

import ViewPort =require ("../../lib/ViewPort");
import Splitter =require ("../../lib/layouts/Splitter");

import Button=require ("../../lib/controls/Button");
import ButtonBar=require ("../../lib/controls/ButtonBar");
import CheckBox=require ("../../lib/controls/CheckBox");
import TextField=require ("../../lib/controls/TextField");
import Range=require ("../../lib/controls/Range");

function fn(){
    /** return (
     <div>12323</div>
     )*/
}

class App extends React.BaseComponent{
    static comp=true;

    render(){
        return new LinearLayout({orientation:"horizontal"},[
            //React.DOM.img({flex:"1",src:"http://www.topoboi.com/pic/201307/1920x1080/topoboi.com-1637.jpg"},null),
            new ListLayout({flex:"1",orientation:"vertical"},[
                React.DOM.img({src:"http://i.pinger.pl/pgr51/a88c0e12001ed7fb4daf11f1/kot+2011172-kot-882-660.jpg"},null),
                React.DOM.img({src:"http://lifeglobe.net/media/entry/1361/kittens_3.jpg"},null),
                React.DOM.img({src:"http://img1.liveinternet.ru/images/attach/c/10/110/188/110188537_4645749_135788470102.jpg"},null),
                React.DOM.img({src:"http://oboi.kards.qip.ru/images/wallpaper/c4/08/133316_800_600.jpg"},null),
                React.DOM.img({src:"http://img1.liveinternet.ru/images/attach/c/7/98/374/98374019_large_t146a5fd9dba78473fb6855a6f71ce90cc.jpg"},null),
                React.DOM.img({src:"http://img1.liveinternet.ru/images/attach/c/2/68/438/68438115_Bankoboev.jpg"},null),
                React.DOM.img({src:"http://cs4180.vkontakte.ru/u10078586/99166631/x_be36cd1b.jpg"},null),
                React.DOM.img({src:"http://s56.radikal.ru/i152/0812/ec/c9dd3207e065.jpg"},null)
            ]),
            new Splitter(null,null),
            new LinearLayout({orientation:"vertical",flex:5},[
                new LinearLayout({orientation:"horizontal",flex:4},[

                    new ListLayout({flex:"3",orientation:"vertical",style:{padding:"10px"}},[
                        new ButtonBar({style:{"margin-top":"15px"}},[
                            new Button({},"hello red"),
                            new Button({},"hello green"),
                            new Button({},"hello blue")
                        ]),
                        new CheckBox({labelPosition:"right","label":"metka",style:{"margin-top":"15px"}},null),
                        new TextField({style:{"margin-top":"15px"}},null),
                        new Range({style:{"margin-top":"15px"}},null)
                    ]),

                    new Splitter(null,null),

                    new MultiFrameLayout({flex:"2",multiframe:"true"},[
                        React.DOM.img({src:"http://www.anypics.ru/pic/201210/1920x1080/anypics.ru-17805.jpg"},null),
                        React.DOM.img({flex:"1",src:"http://img1.liveinternet.ru/images/attach/b/3/7/54/7054370_cats0007wp.jpg"},null),
                        React.DOM.img({flex:"1",src:"http://www.topoboi.com/pic/201307/1920x1080/topoboi.com-1637.jpg"},null),
                        React.DOM.img({flex:"1",src:"http://bashny.net/uploads/images/00/00/35/2013/05/08/7efa4aa489.jpg"},null)
                    ]),

                ]),
                new Splitter(null,null),
                new ListLayout({flex:"1",orientation:"horizontal"},[
                    //React.DOM.img({src:"http://i.pinger.pl/pgr51/a88c0e12001ed7fb4daf11f1/kot+2011172-kot-882-660.jpg"},null),
                    React.DOM.img({style:{},src:"http://s56.radikal.ru/i152/0812/ec/c9dd3207e065.jpg"},null),
                    //React.DOM.img({style:{"min-width":'200px',"width":'200px'},src:"http://s56.radikal.ru/i152/0812/ec/c9dd3207e065.jpg"},null),
                    React.DOM.img({style:{"width":'200px'},src:"http://s56.radikal.ru/i152/0812/ec/c9dd3207e065.jpg"},null),
                    React.DOM.img({style:{"width":'200px'},src:"http://s56.radikal.ru/i152/0812/ec/c9dd3207e065.jpg"},null),
                    React.DOM.img({style:{"width":'200px'},src:"http://s56.radikal.ru/i152/0812/ec/c9dd3207e065.jpg"},null),
                    React.DOM.img({style:{"width":'200px'},src:"http://s56.radikal.ru/i152/0812/ec/c9dd3207e065.jpg"},null),
                    React.DOM.img({style:{"width":'200px'},src:"http://s56.radikal.ru/i152/0812/ec/c9dd3207e065.jpg"},null),
                    React.DOM.img({style:{"width":'200px'},src:"http://s56.radikal.ru/i152/0812/ec/c9dd3207e065.jpg"},null),
                    React.DOM.img({style:{"width":'200px'},src:"http://s56.radikal.ru/i152/0812/ec/c9dd3207e065.jpg"},null),
                    React.DOM.img({style:{"width":'200px'},src:"http://s56.radikal.ru/i152/0812/ec/c9dd3207e065.jpg"},null)
                ])
                ,React.DOM.div({className:"test"},"12312312332")
            ])
        ])
    }

}

var v=new ViewPort(App);
