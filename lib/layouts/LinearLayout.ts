﻿/// <reference path="../module.d.ts" />
import React=require ("../../../reactts/lib/ReactTS");
require("./assets/layouts.css");

class LinearLayout extends React.BaseComponent {

    static getDefaultProps(){
        return{
            orientation:"horizontal"
        }
    }

    /*onLayoutResize=function(x,y){
        console.log("-",x,y);
    }*/

    private getDirection():string {
        var direction="row";
        if(this.props.orientation){
            if(this.props.orientation=="vertical") direction="column"
        }
        return direction;
    }

    //render
    render() {
        this.props.children.forEach((item,count)=> {
            if (item.type.isLayoutSplitter) {
                item.props.layoutLink = this;
                if (count > 0 && count < this.props.children.length) {
                    item.props.leftLink = this.props.children[count - 1];
                    item.props.rightLink = this.props.children[count + 1];
                }
            } else {
                console.log('i',item.props);
                item.props.style = item.props.style || {};
                if (item.props.flex) item.props.style.flex = item.props.style.flex || item.props.flex;
                if (item.props.width) item.props.style.width = item.props.style.width || item.props.width;
                if (item.props.height) item.props.style.height = item.props.style.height || item.props.height
                //console.log("style",item.props.style)
            }
        });

        this.props.style=this.props.style||{};
        this.props.style["flex-direction"]=this.getDirection();

        //return this.transferPropsTo(<div>{'√ '}{this.props.children}</div>);
        return React.DOM.div({style:this.props.style,className:"layout-linear"},this.props.children);
    }


    /*  пока необходимости в ручном контроле стилей н возникало, вероятно и не возникнет
    componentDidUpdate(){
        //TODO вариант1, вопрос как из дескриптора получить ноду?
        //this.props.children.forEach(function(item){
            //console.log(item.props.flex);
        //})

        //TODO вариант 2 как из ноды получить дескриптор
        var childs:any=this.getDOMNode ().childNodes;
        for (var i=0;i<childs.length;i++){
           //var node=ReactMount.getNode(childs[i].getAttribute("data-reactid"));
           //var node=ReactMount.findReactContainerForID(childs[i].getAttribute("data-reactid"));
           //console.log(node)
        }
    }*/

}


React.registerModule(module);
export=LinearLayout;