/**
 * распологает в горизинтальну или вертикальную линию всех детей
 * если дети не входят то появляется скроллинг,
 * дети сами ответственны за совй размер согласно направлению ориентации
 *
 * принимает свойства
 * orientation=horizontal||vertical
 * onScroll= если был скроллинг
 * scrollPos= позиция скролинга
 *
 * предоставляет API
 * setScroll(позиция);
 *
 * генерирует события
 * onScroll
 *
 * приоритет очень высокий (используется в таймлайне, properyeditor и др)
 */

/// <reference path="../module.d.ts" />
import React=require ("../../../reactts/lib/ReactTS");
require("./assets/layouts.css");

class ListLayout extends React.BaseComponent {
    //mixins=[ReactBrowserComponentMixin]

    /*static getDefaultProps() {
        return{
            orientation: "horizontal"
        }
    }*/

    private getDirection():string {
        var direction = "row";
        if (this.props.orientation) {
            if (this.props.orientation == "vertical") direction = "column";
        }
        return direction;
    }

    //render
    render() {
        var childs=[];

        this.props.children.forEach((item, count)=> {
            if (item.type.isLayoutSplitter) {
                item.props.style.display = "none";
            } else {
                childs.push(item);
                item.props.style=item.props.style||{};

                if(this.getDirection()=="row"){
                    if (!item.props.style["min-width"] ){
                        item.props.style["min-width"]=item.props.style["width"];
                    }
                } else {

                }
            }
        });

        this.props.style = this.props.style || {};
        console.log("direction=",this.getDirection());
        return React.DOM.div({
            style: this.props.style,
            className:"layout-list  "+((this.getDirection()==="row")? "layout-list-horizontal":"layout-list-vertical")
        }, childs);
    }
}

React.registerModule(module);
export=ListLayout;