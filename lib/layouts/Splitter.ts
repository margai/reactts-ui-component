//TODO  внешний вид сплиттера должен быть определен в классах

/// <reference path="../module.d.ts" />
import React=require ("../../../reactts/lib/ReactTS")
require("./assets/layouts.css");


class Splitter extends React.BaseComponent {
    private startX=0;
    private startY=0;
    private nowX=0
    private nowY=0
    private offsetX=0
    private offsetY=0
    private startEnvironment:any;
    private orientation:string;

    static isLayoutSplitter=true;

    getInitialState(){
        return{
            isDrag:false
        }
    }

    /**
     *
     */
    private getImmediateEnvironment(el){
        //console.log(,target.nextSibling); 
        var side1Node:any=el.previousSibling,
            side2Node:any=el.nextSibling,
            side1= {
                node: side1Node,
                width: (side1Node) ? side1Node.offsetWidth : 0,
                height: (side1Node) ? side1Node.offsetHeight : 0
            },
            side2={
                node:side2Node,
                width:(side2Node)?side2Node.offsetWidth:0,
                height:(side2Node)?side2Node.offsetHeight:0
            };

        return{
            side1:side1,
            side2:side2,
            width:side1.width+side2.width,
            height:side1.height+side2.height
        }
    }


    private onDragStart(e){
        this.startX=e.screenX;
        this.startY=e.screenY;
        this.startEnvironment=this.getImmediateEnvironment(e.target);

        this.setState({
            isDrag:true
        })

        document.body.addEventListener("mousemove", this.onDrag);
        document.body.addEventListener("mouseup", this.onDragEnd);
    }

    private onDragEnd(e){
        document.body.removeEventListener("mousemove",this.onDrag)
        document.body.removeEventListener("mouseup",this.onDragEnd)

        this.setState({
            isDrag:false
        })
    }

    private onDrag(e){
        var leftSideFlex:number,
            rightSideFlex:number,
            upSideFlex:number,
            downSideFlex:number,
            offsetX=e.screenX-this.startX,
            offsetY=e.screenY-this.startY,
            side1:any,
            side2:any,
            sumFlex:number;

        if(this.props.leftLink && this.props.rightLink ){
            side1=this.props.leftLink;
            side2=this.props.rightLink;
            sumFlex=(1*side1.props.flex + 1*side2.props.flex);

            if (this.orientation=="horizontal") {
                leftSideFlex = (sumFlex / this.startEnvironment.width) * (this.startEnvironment.side1.width + offsetX);
                rightSideFlex = (sumFlex / this.startEnvironment.width) * (this.startEnvironment.side2.width - offsetX);
                this.startEnvironment.side1.node.style.flex = leftSideFlex;
                this.startEnvironment.side2.node.style.flex = rightSideFlex;
                this.props.leftLink.props.flex = leftSideFlex;
                this.props.rightLink.props.flex = rightSideFlex;
            } else {
                upSideFlex = (sumFlex / this.startEnvironment.height) * (this.startEnvironment.side1.height + offsetY);
                downSideFlex = (sumFlex / this.startEnvironment.height) * (this.startEnvironment.side2.height - offsetY);
                this.startEnvironment.side1.node.style.flex = upSideFlex;
                this.startEnvironment.side2.node.style.flex = downSideFlex;
                this.props.leftLink.props.flex = upSideFlex;
                this.props.rightLink.props.flex = downSideFlex;
            }
            //this.props.layoutLink.forceUpdate(function(){});
        }
    }

    componentWillMount() {
        this.onDragEnd=this.onDragEnd.bind(this);
        this.onDrag=this.onDrag.bind(this);
        if(this.props.layoutLink){
            this.orientation=this.props.layoutLink.props.orientation;
        }
    }


    //render
    render() {
        var width="",height="";
        var orientationClassName=(this.props.layoutLink.props.orientation=="horizontal")?"layout-splitter-horizontal":"layout-splitter-vertical";

        return React.DOM.div({
            style:{
                background:this.state.isDrag==false?"#f1f1f1":"black",
                cursor:this.orientation=="vertical"?'n-resize':'w-resize'
            },
            className:"layout-splitter "+orientationClassName,
            onMouseDown:this.onDragStart.bind(this)
            //onMouseUp:this.onDragEnd.bind(this)
            //onDragStart:this.onDragStart.bind(this),
            //onDragEnd:this.onDragEnd.bind(this)
            //onDrag:this.onDrag.bind(this)
        },this.props.children);
    }
}

React.registerModule(module);
export=Splitter;