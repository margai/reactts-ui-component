/**
 * Расставляет несколько слоев друг под другом
 * если задействовать left, top, width,height то поведение будет аналогично absolute layer
 */

/// <reference path="../module.d.ts" />
import React=require ("../../../reactts/lib/ReactTS")


class MultiFrameLayout extends React.BaseComponent {

    /*getDefaultProps(){
        alert('get default');
        return{
            style:{
                display:"flex"
            }
        }
    }*/
    select=0;

    componentWillMount() {
        setInterval(()=>{
            var n=Math.round(Math.random() *3);
            //console.log(n);
            this.select=n;
            this.forceUpdate(function(){});
        },1000);
    }

    //render
    render() {
        this.props.children.forEach((item,count)=> {
            item.props.style = item.props.style || {};

            //TODO подумать нужно ли заморачиваться ос стилями, или можно указвть координаты детей в классах
            /*item.props.style.left=item.props.style.left||"0px";
            item.props.style.right=item.props.style.right||"0px";
            item.props.style.top=item.props.style.top||"0px";
            item.props.style.bottom=item.props.style.bottom||"0px";
            item.props.style.width=item.props.style.width||"100%";
            item.props.style.height=item.props.style.height||"100%";*/

            //item.props.style.position="absolute";
            item.props.style.display=(this.select==count)?"block":"none";
        });

        return React.DOM.div({style:this.props.style,className:"layout-multiframe"},[
            React.DOM.div({className:"layout-multiframe-inner"},this.props.children)
        ]);
    }
}

React.registerModule(module);
export=MultiFrameLayout;