/*
 * Icon button
 *
 * type (quiet, large, large--quiet)
 * which
 */
var iconButton = React.createClass({
    render: function () {
        return this.transferPropsTo(d.button(
            { className: 'topcoat-icon-button' + (this.props.type ? '--' + this.props.type : '') },
            tc.icon({
                which: this.props.which,
                type: (this.props.type && this.props.type.match(/large/) ? 'large' : null)
            })
        ));
    }
});
