/*
 * Checkbox
 *
 * labelPosition (left, right)
 * label
 */

/// <reference path="../module.d.ts" />
import React=require ("../../../reactts/lib/ReactTS");
//require("../../themes/css/topcoat-desktop-dark.css");

class CheckBox extends React.BaseComponent {
    getDefaultProps() {
        return {
            labelPosition: 'right'
        };
    }

    render() {
        var style=this.props.style||{};
        this.props.style={};
        var input = this.transferPropsTo(React.DOM.input({
            type: 'checkbox'
        }));
        //alert(this.props.labelPosition);
        return React.DOM.label(
            {
                className: 'topcoat-checkbox',
                "style": style
            },
            (this.props.labelPosition === 'left' ? [this.props.label, ' '] : null),
            input,
            React.DOM.div({ className: 'topcoat-checkbox__checkmark' }),
            (this.props.labelPosition === 'right' ? [' ', this.props.label] : null)
        );
    }
}
React.registerModule(module);
export=CheckBox;

