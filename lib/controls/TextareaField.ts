/*
 * Text area
 *
 * type (large)
 */
var textArea = React.createClass({
    render: function() {
        return this.transferPropsTo(d.textarea(
            {
                className: 'topcoat-textarea' + (this.props.type === 'large' ? '--large' : '')
            }
        ));
    }
});