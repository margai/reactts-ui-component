/*
 * Button
 *
 * type (quiet, large, large--quiet, cta, large--cta)
 */

/// <reference path="../module.d.ts" />
import React=require ("../../../reactts/lib/ReactTS");

class Button extends React.BaseComponent {
    //render
    render() {
        return this.transferPropsTo(React.DOM.button(
            { className: 'topcoat-button' + (this.props.type ? '--' + this.props.type : '') },
            this.props.children
        ));
    }
}

React.registerModule(module);
export=Button;