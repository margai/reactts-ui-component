/*
 * List
 *
 * header
 */
var list = React.createClass({
    render: function() {
        var itemKey = 0;
        return this.transferPropsTo(d.div(
            { className: 'topcoat-list' },
            (this.props.header ? d.h3({ className: 'topcoat-list__header' }, this.props.header) : null),
            d.ul(
                { className: 'topcoat-list__container' },
                _map(this.props.children, function(child) {
                    child.props.className = child.props.className + ' topcoat-list__item';
                    child.props.key = (itemKey++).toString();
                    return child;
                })
            )
        ));
    }
});