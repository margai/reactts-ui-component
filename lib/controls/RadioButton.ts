/*
 * Radio button
 *
 * labelPosition (left, right)
 * label
 */
var radioButton = React.createClass({
    getDefaultProps: function() {
        return {
            labelPosition: 'right'
        };
    },
    render: function() {
        var input = this.transferPropsTo(d.input({
            type: 'radio'
        }));
        return d.label(
            { className: 'topcoat-radio-button' },
            (this.props.labelPosition === 'left' ? [this.props.label, ' '] : null),
            input,
            d.div({ className: 'topcoat-radio-button__checkmark' }),
            (this.props.labelPosition === 'right' ? [' ', this.props.label] : null)
        );
    }
});