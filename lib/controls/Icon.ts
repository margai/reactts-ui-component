/*
 * Icon
 *
 * which
 * type (large)
 */
var icon = React.createClass({
    render: function() {
        return d.span({
            className: (this.props.type === 'large' ? 'topcoat-icon--large' : 'topcoat-icon') + ' topcoat-icon--' + this.props.which
        });
    }
});