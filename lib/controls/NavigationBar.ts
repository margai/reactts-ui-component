/*
 * Navigation bar
 *
 * leftButton
 * title
 * rightButton
 */
var navigationBar = React.createClass({
    render: function() {
        return this.transferPropsTo(d.div(
            { className: 'topcoat-navigation-bar' },
            d.div(
                { className: 'topcoat-navigation-bar__item left quarter' },
                this.props.leftButton
            ),
            d.div(
                { className: 'topcoat-navigation-bar__item center half' },
                d.h1(
                    { className: 'topcoat-navigation-bar__title' },
                    this.props.title
                )
            ),
            d.div(
                { className: 'topcoat-navigation-bar__item right quarter' },
                this.props.rightButton
            )
        ));
    }
});
