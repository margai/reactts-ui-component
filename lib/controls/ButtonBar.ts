/*
 * Button bar
 *
 * type (large)
 */


/*
 * Button
 *
 * type (quiet, large, large--quiet, cta, large--cta)
 */

/// <reference path="../module.d.ts" />
import React=require ("../../../reactts/lib/ReactTS");
//require("../../themes/css/topcoat-desktop-dark.css");

class ButtonBar extends React.BaseComponent {
    //render
    render() {
        var buttonClass = (this.props.type === 'large' ? ' topcoat-button-bar__button--large' : ' topcoat-button-bar__button');
        var buttonKey = 0;
        return this.transferPropsTo(React.DOM.div(
            { className: 'topcoat-button-bar' },
            this.props.children.map( function(child) {

                child.props.className = child.props.className + buttonClass;
                return React.DOM.div (
                    {
                        className: 'topcoat-button-bar__item',
                        key: (buttonKey++).toString()
                    },
                    child
                );

            })
        ));
    }
}
React.registerModule(module);
export=ButtonBar;