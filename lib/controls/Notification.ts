/*
 * Notification
 */
var notification = React.createClass({
    render: function() {
        return this.transferPropsTo(d.span(
            { className: 'topcoat-notification' },
            this.props.children
        ));
    }
});