/*
 * Switch
 */
tc.switch = React.createClass({
    render: function() {
        var input = this.transferPropsTo(d.input({
            type: 'checkbox',
            className: 'topcoat-switch__input'
        }));
        return d.label(
            { className: 'topcoat-switch' },
            input,
            d.div({ className: 'topcoat-switch__toggle' })
        );
    }
});