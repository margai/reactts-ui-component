/*
 * Text input
 * type (large)
 */
/// <reference path="../module.d.ts" />
import React=require ("../../../reactts/lib/ReactTS");
class TextField extends React.BaseComponent {
    //render
    render() {
        return this.transferPropsTo(React.DOM.input({
            type: 'text',
            className: 'topcoat-text-input' + (this.props.type === 'large' ? '--large' : '')
        }));
    }
}

React.registerModule(module);
export=TextField;

/*tc.textInput = React.createClass({
    render: function() {
        return this.transferPropsTo(d.input({
            type: 'text',
            className: 'topcoat-text-input' + (this.props.type === 'large' ? '--large' : '')
        }));
    }
});*/