/*
 * Search input
 *
 * type (large)
 */
tc.searchInput = React.createClass({
    render: function() {
        return this.transferPropsTo(d.input({
            type: 'search',
            className: 'topcoat-search-input' + (this.props.type === 'large' ? '--large' : '')
        }));
    }
});