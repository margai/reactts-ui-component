/*
 * Range
 */

/// <reference path="../module.d.ts" />
import React=require ("../../../reactts/lib/ReactTS");

class Range extends React.BaseComponent {
    //render
    render() {
        return this.transferPropsTo(React.DOM.input({
            type: 'range',
            className: 'topcoat-range'
        }));
    }
}

React.registerModule(module);
export=Range;