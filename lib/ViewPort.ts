﻿/// <reference path="./module.d.ts" />
import React=require ("../../reactts/lib/ReactTS")

class ViewPort {
    container:any;

    constructor(app) {

        if (app["prototype"]["render"]) {
            app=new (React.registerModule["createComponent"](app))({},null);
        }

        app.props=app.props||{};
        app.props.style = app.props.style || {flex: 1};

        //React.renderComponent(React.DOM.div({id: "appcontainer"}, app), document.body);
        React.renderComponent(app, document.body);
        this.container=document.body.firstChild;//document.getElementById("appcontainer");
	   	
        window.addEventListener("resize",this.resize.bind(this));
        setTimeout(this.resize.bind(this),1);    
    }

    getClientHeight(){
        var height=document.compatMode=='CSS1Compat' && !window["opera"]?document.documentElement.clientHeight:document.body.clientHeight;
        return height;
    }

    resize (){
        console.log(this.container);
	    this.container.style.height=this.getClientHeight()+"px";
        this.container.style.display="flex";
    }
}
export=ViewPort;